-- Check if the variant is a Jumblegut one.
---@param entity EntityNPC
---@return string | boolean
function Jumbleguts:IsJumblegut(entity)
    for i, variant in pairs(Jumbleguts.Variants) do
        if entity.Variant == variant then
            return i
        end
    end
    return false
end


-- Linear interpolate from one number / vector to another by `percent` decimal.
---@param first number | Vector
---@param second number | Vector
---@param percent number
---@return number | Vector
function Jumbleguts:Lerp(first, second, percent)
	return (first + (second - first) * percent)
end

-- Lerp to `Vector.Zero`.
---@param vector Vector
---@param percent number? Defaults to `0.25`.
---@return Vector
function Jumbleguts:StopLerp(vector, percent)
	---@type Vector
	return Jumbleguts:Lerp(vector, Vector.Zero, percent or 0.25)
end


-- Better sound function.
---@param entity EntityNPC? Optionally provide `nil` to play sound from SFXManager.
---@param id integer
---@param volume number? Default is `1`.
---@param pitch number? Default is `1`.
---@param cooldown integer? Also known as `FrameDelay`, default is `0`.
---@param loop boolean? Default is `false`.
---@param pan number? Default is `0`.
function Jumbleguts:PlaySound(entity, id, volume, pitch, cooldown, loop, pan)
	volume = volume or 1
	pitch = pitch or 1
	cooldown = cooldown or 0
	pan = pan or 0

	if entity then
		entity:ToNPC():PlaySound(id, volume, cooldown, loop or false, pitch)
	else
		SFXManager():Play(id, volume, cooldown, loop, pitch, pan)
	end
end


-- General RNG function.
---@param min number? You may only omit this if you're also omitting `max`, which will make it use `RandomFloat()`.
---@param max number? Omit when providing a `min` for `rng:RandomInt(min + 1)`.
---@param rng RNG? Omit to use a generic RNG object.
function Jumbleguts:Random(min, max, rng)
	rng = rng or Jumbleguts.RNG

	-- Float
	if not min and not max then
		return rng:RandomFloat()

	-- Integer
	elseif min and not max then
		return rng:RandomInt(min + 1)

	-- Range
	else
		local difference = math.abs(min)

		-- For ranges with negative numbers
		if min < 0 then
			max = max + difference
			return rng:RandomInt(max + 1) - difference
		-- For positive only
		else
			max = max - difference
			return rng:RandomInt(max + 1) + difference
		end
	end
end


-- Get a vector with a random angle.
---@param length number? Omit to get a unit vector.
function Jumbleguts:RandomVector(length)
	local vector = Vector.FromAngle(Jumbleguts:Random(359))
	if length then
		vector = vector:Resized(length)
	end
	return vector
end


-- Spawn creep easily.
---@param type number Creep variant.
---@param spawner Entity | nil Creep spawner.
---@param position Vector Spawn position.
---@param scale number? Sprite scale, defaults to `1`.
---@param timeout number? Defaults to `124`.
---@return EntityEffect
function Jumbleguts:QuickCreep(type, spawner, position, scale, timeout)
	---@type EntityEffect
	---@diagnostic disable-next-line: assign-type-mismatch
	local creep = Isaac.Spawn(EntityType.ENTITY_EFFECT, type, 0, position, Vector.Zero, spawner):ToEffect()
	creep.SpriteScale = Vector(scale or 1, scale or 1)

	if timeout then
		creep:SetTimeout(timeout)
	end

	creep:Update()
	return creep
end


-- Turn red creep into boiling creep.
---@param creep EntityEffect
---@param spawner EntityNPC The parent of the creep.
function Jumbleguts:TurnCreepBoiling(creep, spawner)
    creep:GetData().JumblegutsBoilingCreep = Jumbleguts:Random(30, 60)
    creep.Parent = spawner

	local backdrop = Game():GetRoom():GetBackdropType()
	if backdrop ~= BackdropType.WOMB and backdrop ~= BackdropType.UTERO then
    	creep:GetSprite().Color = Jumbleguts.Colors.ClottedGibs
	end
end