local mod = Jumbleguts

mod.RNG = RNG()

-- Randomize the seed, since RNG is always initialized at 2853650767
mod:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function ()
	mod.RNG:SetSeed(Game():GetSeeds():GetStartSeed())
end)



--[[ New entity enums ]]--
mod.JumblegutsType = Isaac.GetEntityTypeByName("Waning Jumbleguts")

mod.Variants = {
    Waning     = Isaac.GetEntityVariantByName("Waning Jumbleguts"),
    Clotted    = Isaac.GetEntityVariantByName("Clotted Jumbleguts"),
    Hollowing  = Isaac.GetEntityVariantByName("Hollowing Jumbleguts"),
    Flummoxing = Isaac.GetEntityVariantByName("Flummoxing Jumbleguts"),
}

mod.BoomerangVariant = Isaac.GetEntityVariantByName("Waning Jumblegut Boomerang")



--[[ Colors ]]--
mod.Colors = {
    WaningGibs     = Color(0,0,0, 1, 0.75,0.6,0),
    ClottedGibs    = Color(0.8,0.8,0.8, 1),
    HollowingGibs  = Color(0,0,0, 1, 0.54,0.64,0.78),
    FlummoxingGibs = Color(0.6,0.6,1, 1, 0.25,0.1,0.25),

    TearEffect = Color(0,0,0, 0.65, 0.54,0.64,0.78),
    TearTrail  = Color(0,0,0, 1, 0.54,0.64,0.78),
    FearCreep  = Color(0.35,0.15,0.35, 1, 0.25,0.1,0.25),
    FearSmoke  = Color(1,0.15,0.35, 0.4, 0.25,0.1,0.25),
}



--[[ New sound enums ]]--
mod.Sounds = {
	Appear = Isaac.GetSoundIdByName("Jumbleguts Appear"),
	Shoot  = Isaac.GetSoundIdByName("Jumbleguts Shoot"),
	HollowingAppear = Isaac.GetSoundIdByName("Jumbleguts Appear (Hollowing)"),
	HollowingShoot  = Isaac.GetSoundIdByName("Jumbleguts Shoot (Hollowing)"),
}