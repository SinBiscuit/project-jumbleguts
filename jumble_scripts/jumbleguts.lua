local mod = Jumbleguts

local Settings = {
    MoveCooldown = 15,
    DashSpeed = 8,
    HollowingDashSpeed = 10,
    BackAwayDistance = 120,
    AttackCooldown = {1, 4},
    HollowingCreepDuration = 75,
}



function mod:JumblegutsInit(entity)
    if mod:IsJumblegut(entity) ~= false then
        entity.SplatColor = mod.Colors[mod:IsJumblegut(entity) .. "Gibs"]

        -- Dash and attack cooldowns
        entity.StateFrame = Settings.MoveCooldown
        entity.ProjectileCooldown = mod:Random(Settings.AttackCooldown[1], Settings.AttackCooldown[2])

        -- Appear sound
        local sound = entity.Variant == mod.Variants.Hollowing and mod.Sounds.HollowingAppear or mod.Sounds.Appear
		mod:PlaySound(entity, sound, 3.5)
    end
end
mod:AddCallback(ModCallbacks.MC_POST_NPC_INIT, mod.JumblegutsInit, mod.JumblegutsType)

function mod:JumblegutsUpdate(entity)
    if mod:IsJumblegut(entity) ~= false then
        local sprite = entity:GetSprite()
        local target = entity:GetPlayerTarget()

        -- Faux slipperiness
        local lerpStep = 0.1
        if entity.Variant == mod.Variants.Waning or entity.Variant == mod.Variants.Hollowing then
            lerpStep = 0.05
        end



        -- Leave behind creep
        local creepSpawnInterval = entity.Variant == mod.Variants.Clotted and 8 or 4

        if entity:IsFrame(creepSpawnInterval, 0) then
            -- Get the type and color of creep
            local creepType = EffectVariant.CREEP_RED
            local color
            local duration

            -- Slippery creep
            if entity.Variant == mod.Variants.Waning or entity.Variant == mod.Variants.Hollowing then
                creepType = EffectVariant.CREEP_SLIPPERY_BROWN

                -- Hollowing
                if entity.Variant == mod.Variants.Hollowing then
                    color = mod.Colors.TearTrail
                    duration = Settings.HollowingCreepDuration

                -- Waning
                else
                    color = mod.Colors.WaningGibs
                end

            -- Curse creep
            elseif entity.Variant == mod.Variants.Flummoxing then
                color = mod.Colors.FearCreep
            end


            local creep = mod:QuickCreep(creepType, entity, entity.Position, 1.25, duration)
            -- Apply color
            if color then
                creep:GetSprite().Color = color
            end

            -- Apply data
            -- Clotted
            if entity.Variant == mod.Variants.Clotted then
                mod:TurnCreepBoiling(creep, entity)

            -- Flummoxing
            elseif entity.Variant == mod.Variants.Flummoxing then
                creep:GetData().JumblegutsCurseCreep = true
            end
        end



        --[[ Change out of the init state ]]--
        if entity.State == NpcState.STATE_INIT and not sprite:IsPlaying("Appear") then
            entity.State = NpcState.STATE_IDLE

        --[[ Idle ]]--
        elseif entity.State == NpcState.STATE_IDLE then
            entity.Velocity = mod:StopLerp(entity.Velocity, lerpStep)

			-- Idle animation
            if not sprite:IsPlaying("Idle") then
                sprite:Play("Idle", true)
            end

            if entity.StateFrame <= 0 then
                entity.StateFrame = Settings.MoveCooldown

                -- Attack
                if entity.ProjectileCooldown <= 0 then
                    entity.State = NpcState.STATE_ATTACK
                    sprite:Play("Attack", true)
                    entity.ProjectileCooldown = mod:Random(Settings.AttackCooldown[1], Settings.AttackCooldown[2])

                -- Move
                else
                    entity.State = NpcState.STATE_MOVE
                    sprite:Play("Dash", true)
                    entity.ProjectileCooldown = entity.ProjectileCooldown - 1
                end

            else
                entity.StateFrame = entity.StateFrame - 1
            end



        --[[ Dash ]]--
        elseif entity.State == NpcState.STATE_MOVE then
            entity.Velocity = mod:StopLerp(entity.Velocity, lerpStep)

            if sprite:IsEventTriggered("Dash") then
                -- Get the dash direction
                local vector = (target.Position - entity.Position)

                -- Confused
                if entity:HasEntityFlags(EntityFlag.FLAG_CONFUSION) then
                    vector = mod:RandomVector()

                -- Feared / too close / doesn't have a path to the target
                elseif entity:HasEntityFlags(EntityFlag.FLAG_FEAR) or entity:HasEntityFlags(EntityFlag.FLAG_SHRINK)
                or entity.Position:Distance(target.Position) <= Settings.BackAwayDistance
				or Game():GetRoom():CheckLine(entity.Position, target.Position, LineCheckMode.RAYCAST, 0, false, false) == false then
                    local rotation = mod:Random(-45, 45)
                    vector = (entity.Position - target.Position):Rotated(rotation)
                end

                local speed = entity.Variant == mod.Variants.Hollowing and Settings.HollowingDashSpeed or Settings.DashSpeed
                entity.Velocity = vector:Resized(speed)

				mod:PlaySound(nil, SoundEffect.SOUND_MEAT_IMPACTS, 0.75, math.random(90, 110) / 100)
            end

            if sprite:IsFinished() then
                entity.State = NpcState.STATE_IDLE
            end



        --[[ Attack ]]--
        elseif entity.State == NpcState.STATE_ATTACK then
            entity.Velocity = mod:StopLerp(entity.Velocity, lerpStep)

            if sprite:IsEventTriggered("Shoot") then
                local params = ProjectileParams()
                local targetVector = (target.Position - entity.Position)

                -- Sound
                local sound = entity.Variant == mod.Variants.Hollowing and mod.Sounds.HollowingShoot or mod.Sounds.Shoot
                mod:PlaySound(entity, sound, 3.5, 1, 2)


                -- Waning
                if entity.Variant == mod.Variants.Waning then
					params.Variant = mod.BoomerangVariant
                    params.BulletFlags = (ProjectileFlags.DECELERATE | ProjectileFlags.NO_WALL_COLLIDE)
                    params.ChangeFlags = (ProjectileFlags.ACCELERATE | ProjectileFlags.NO_WALL_COLLIDE | ProjectileFlags.MOVE_TO_PARENT)
                    params.Acceleration = 1.05

                    local projectile = entity:FireProjectilesEx(entity.Position, targetVector:Resized(15), ProjectileMode.SINGLE, params)[1]
                    projectile.Parent = entity
                    projectile:GetData().JumblegutsBoomerangShot = false
                    projectile:GetData().Height = projectile.Height

					mod:PlaySound(nil, SoundEffect.SOUND_SWORD_SPIN, 0.75)


                -- Clotted
                elseif entity.Variant == mod.Variants.Clotted then
                    params.Scale = 2.5
                    params.FallingSpeedModifier = 1
                    params.FallingAccelModifier = -0.16
                    params.BulletFlags = ProjectileFlags.DECELERATE
                    params.Acceleration = 1.06

                    local projectile = entity:FireProjectilesEx(entity.Position, targetVector:Resized(10), ProjectileMode.SINGLE, params)[1]
                    projectile:GetData().JumblegutsCreepShot = true
                    projectile.Parent = entity

					mod:PlaySound(nil, SoundEffect.SOUND_BLOODSHOOT, 1.8)


                -- Hollowing
                elseif entity.Variant == mod.Variants.Hollowing then
                    params.Variant = ProjectileVariant.PROJECTILE_TEAR
                    params.FallingSpeedModifier = 1
					params.FallingAccelModifier = -0.1
                    params.HeightModifier = -10

                    local projectile = entity:FireProjectilesEx(entity.Position, targetVector:Resized(13), ProjectileMode.SINGLE, params)[1]
                    projectile:GetData().JumblegutsHollowingShot = true

					-- Add a trail to the projectile
					local trail = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.SPRITE_TRAIL, 0, projectile.Position, Vector(0, projectile.Height), projectile):ToEffect()
					trail.Parent = projectile
					projectile:GetData().spriteTrail = trail
					trail.Color = mod.Colors.TearTrail
					trail.SpriteScale = Vector.One * 2
					trail:Update()

					mod:PlaySound(nil, SoundEffect.SOUND_TEARS_FIRE, 1.25)


                -- Flummoxing
                elseif entity.Variant == mod.Variants.Flummoxing then
                    params.Scale = 1.5
                    params.FallingSpeedModifier = 1
					params.FallingAccelModifier = -0.09
                    params.BulletFlags = ProjectileFlags.CONTINUUM
                    entity:FireProjectilesEx(entity.Position, Vector(10, 4), ProjectileMode.CROSS, params)

					mod:PlaySound(nil, SoundEffect.SOUND_BISHOP_HIT)
                end
            end

            if sprite:IsFinished() then
                entity.State = NpcState.STATE_IDLE
            end
        end
    end
end
mod:AddCallback(ModCallbacks.MC_NPC_UPDATE, mod.JumblegutsUpdate, mod.JumblegutsType)