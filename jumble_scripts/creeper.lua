local mod = Jumbleguts



--[[ Projectiles ]]--
-- Boomerang shot
function mod:JumblegutBoomerangUpdate(projectile)
    local sprite = projectile:GetSprite()
	local data = projectile:GetData()

    if not sprite:IsPlaying("Move") then
        sprite:Play("Move", true)
    end

    -- Rotate
    local rotation = math.max(10, projectile.Velocity:Length() * 4)
    sprite.Rotation = sprite.Rotation + rotation


	if data.JumblegutsBoomerangShot ~= nil and projectile.Parent then
        -- Change flags and go back to the parent
        if data.JumblegutsBoomerangShot == false and projectile.Velocity:Length() <= 2 then
           data.JumblegutsBoomerangShot = true
           projectile.ProjectileFlags = projectile.ChangeFlags
           projectile.Acceleration = 1.08
        end

        -- Don't fall down
        projectile.Height = data.Height
        projectile.FallingSpeed = 0
        projectile.FallingAccel = 0
	end
end
mod:AddCallback(ModCallbacks.MC_POST_PROJECTILE_UPDATE, mod.JumblegutBoomerangUpdate, mod.BoomerangVariant)

function mod:JumblegutBoomerangPop(projectile)
    local effect = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BULLET_POOF, 0, projectile.Position, Vector.Zero, projectile):GetSprite()
	effect.Offset = Vector(projectile.PositionOffset.X, projectile.PositionOffset.Y * 0.65)
	effect.Color = mod.Colors.WaningGibs
end
mod:AddCallback(ModCallbacks.MC_POST_PROJECTILE_DEATH, mod.JumblegutBoomerangPop, mod.BoomerangVariant)



-- Creep shot
function mod:JumblegutCreepProjectileUpdate(projectile)
    if projectile:GetData().JumblegutsCreepShot then
        -- Creep
        if projectile.Parent and projectile:IsFrame(8, 0) then
            local creep = mod:QuickCreep(EffectVariant.CREEP_RED, projectile.Parent, projectile.Position)
            mod:TurnCreepBoiling(creep, projectile.Parent)
        end


        -- Haemo particle trail
		if projectile:IsFrame(2, 0) then
			local trail = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.HAEMO_TRAIL, 0, projectile.Position, Vector.Zero, projectile):ToEffect()
			trail.DepthOffset = projectile.DepthOffset - 10

			-- Trail behind the projectile
			trail.Velocity = -projectile.Velocity:Resized(2)
			trail.SpriteOffset = Vector(projectile.PositionOffset.X, projectile.Height * 0.65)

			-- Scale
			local scaler = projectile.Scale * 0.5 + (math.random(-10, 10) / 100)
			trail.SpriteScale = Vector(scaler, scaler)

			-- Custom offset
			local c = mod.Colors.ClottedGibs
			local colorOffset = math.random(-1, 1) * 0.06
			trail:GetSprite().Color = Color(c.R,c.G,c.B, 1, c.RO + colorOffset, c.GO + colorOffset, c.BO + colorOffset)

			trail:Update()
		end
	end
end
mod:AddCallback(ModCallbacks.MC_POST_PROJECTILE_UPDATE, mod.JumblegutCreepProjectileUpdate, ProjectileVariant.PROJECTILE_NORMAL)



-- Grid destroying shot
function mod:JumblegutTearProjectileUpdate(projectile)
    local data = projectile:GetData()

    if projectile:GetData().JumblegutsHollowingShot then
        data.spriteTrail.Velocity = projectile.Position + projectile.PositionOffset - data.spriteTrail.Position

        -- Destroy grids
        if projectile:CollidesWithGrid() then
            local room = Game():GetRoom()
            local size = projectile.Scale * projectile.Size
            local grindex = room:GetGridIndex(projectile.Position + projectile.Velocity:Resized(size))

            -- Effects when it destroyed a grid
            if room:DestroyGrid(grindex, true) == true then
                mod:PlaySound(nil, SoundEffect.SOUND_BOSS2INTRO_WATER_EXPLOSION, 0.4)

                local effect = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BLOOD_EXPLOSION, 3, room:GetGridPosition(grindex), Vector.Zero, projectile):ToEffect()
	            local effectSprite = effect:GetSprite()
                effectSprite.Color = mod.Colors.TearEffect
            end
        end
	end
end
mod:AddCallback(ModCallbacks.MC_POST_PROJECTILE_UPDATE, mod.JumblegutTearProjectileUpdate, ProjectileVariant.PROJECTILE_TEAR)





--[[ Creep types ]]--
function mod:JumblegutCreepUpdate(effect)
    if effect.Timeout >= 10 then
        local data = effect:GetData()

        --[[ Boiling blood ]]--
        if data.JumblegutsBoilingCreep and effect.Parent then
            local room = Game():GetRoom()

            -- Make the boiling work in flooded Scarred Womb rooms
            if room:HasWater() and room:GetBackdropType() == BackdropType.SCARRED_WOMB then
                effect.SpriteScale = Vector.One
                effect.State = 0
                effect.Visible = false
            end


            -- Projectiles
            if effect:IsFrame(data.JumblegutsBoilingCreep, 0) then
                local size = mod:Random(0, effect.Size)
                local offset = mod:RandomVector(size)
                local params = ProjectileParams()
                params.FallingAccelModifier = 1.25
                params.FallingSpeedModifier = -10
                effect.Parent:ToNPC():FireProjectilesEx(effect.Position + offset, mod:RandomVector(2), 0, params)

                -- Epic bubbly sounds
                mod:PlaySound(nil, SoundEffect.SOUND_BOSS2_BUBBLES, 0.35, math.random(90, 110) / 100)
            end


            -- Blood bubbles
            if effect:IsFrame(math.floor(data.JumblegutsBoilingCreep / 2), 0) then
                local size = mod:Random(0, math.floor(effect.Size * 0.8))
                local offset = mod:RandomVector(size)

                local bubble = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.TAR_BUBBLE, 0, effect.Position + offset, Vector.Zero, effect)
                bubble:GetData().JumblegutsBoilingBubble = true
                bubble:GetSprite():ReplaceSpritesheet(0, "gfx/effects/blood_bubble.png", true)
            end



        --[[ Fear creep ]]--
        elseif data.JumblegutsCurseCreep then
            -- Fear players standing in the creep
            for i, player in pairs(PlayerManager.GetPlayers()) do
                if player.Position:Distance(effect.Position) <= effect.Size then
                    player:AddFear(EntityRef(effect.SpawnerEntity), 2)
                    player:SetFearCountdown(30) -- Reset the timer instead of adding onto it
                end
            end

            -- Smoke effects
            if effect:IsFrame(math.random(20, 40), 0) then
                local offset = mod:RandomVector(mod:Random(0, effect.Size))
                local vector = Vector.FromAngle(-90):Rotated(math.random(-30, 30)):Resized(math.random(10, 20) / 10)
                local smoke = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.DARK_BALL_SMOKE_PARTICLE, 0, effect.Position + offset, vector, effect):ToEffect()

                local sprite = smoke:GetSprite()
                sprite.PlaybackSpeed = 0.3
                sprite.Scale = Vector.One * math.random(60, 100) / 100
                sprite.Color = mod.Colors.FearSmoke

                smoke:Update()
            end
        end
    end
end
mod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, mod.JumblegutCreepUpdate, EffectVariant.CREEP_RED)

-- Don't take damage from fear creep
function mod:JumblegutCreepHit(entity, damageAmount, damageFlags, damageSource, damageCountdownFrames)
	if damageSource.Type == EntityType.ENTITY_EFFECT and damageSource.Variant == EffectVariant.CREEP_RED
    and damageSource.Entity:GetData().JumblegutsCurseCreep then
		return false
	end
end
mod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, mod.JumblegutCreepHit)