Jumbleguts = RegisterMod("Jumbleguts", 1)

--[[ Load the scripts ]]--
local scriptFolder = "jumble_scripts."
include(scriptFolder .. "library")
include(scriptFolder .. "constants")
include(scriptFolder .. "jumbleguts")
include(scriptFolder .. "creeper")